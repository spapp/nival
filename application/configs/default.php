<?php
return array(
    'controller' => array(
        'path' => APPLICATION_PATH . '/controllers',
        'defaultController' => 'comment',
        'defaultAction' => 'list'
    ),
    'layout' => array('path' => APPLICATION_PATH . '/layouts',
        'default' => 'layout'
    ),
    'view' => array(
        'path' => APPLICATION_PATH . '/views',
        'css' => array('skins/default/main.css')
    ),
    'database' => array(
        // az első (master) adatbázis konfigja
        'master' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'nival'
            )
        ),
        // a második adatbázis konfigja
        'slave0' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'nival'
            )
        ),
        // a harmadik adatbázis konfigja
        'slave1' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'nival'
            )
        )
    )
);
