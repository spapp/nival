<?php
class CommentController extends Controller {
    public function init() {
        $action = $this->request->getActionName();

        if (!Auth::authenticated() and $action != 'list') {
            $this->redirect('list');
        }
    }

    public function listAction() {
        $model = $this->getModel('comment');

        $this->view->comments = $model->fetchAll();

    }

    public function addAction() {
        $comment = $this->request->getPost('comment', null);

        if ($comment) {
            $model = $this->getModel('comment');
            $model->save(array(
                                  'comment' => strip_tags($comment),
                                  'date'    => date('Y-m-d H:i:s'),
                                  'user_id' => Auth::get('id')
                              ));
        }

        $this->redirect('list');
    }
}
