<?php
class UserController extends Controller {

    public function init() {
        $action = $this->request->getActionName();

        if (Auth::authenticated() and $action != 'signout') {
            $this->redirect('list', 'comment');
        } elseif (!Auth::authenticated() and $action == 'signout') {
            $this->redirect('list', 'comment');
        }
    }

    public function signinAction() {
        $email = $this->request->getPost('email', null);
        $password = $this->request->getPost('password', null);

        if ($email and $password) {
            $model = $this->getModel('user');

            $userData = $model->fetchOne(array(
                'email' => strip_tags($email),
                'password' => UserModel::hash($password),
            ));

            if (isset($userData) and isset($userData->id)) {
                Auth::write((array)$userData);
                $this->redirect('list', 'comment');
            }
        }
    }

    public function signoutAction() {
        Auth::clear();
        $this->redirect('list', 'comment');
    }

    public function signupAction() {
        $name = $this->request->getPost('name', null);
        $email = new Email($this->request->getPost('email', null));
        $password = $this->request->getPost('password', null);

        // FIXME adat ellenőrzés (Form object)
        // e-mail validálás
        if ($name and $email->isValid() and $password) {
            $model = $this->getModel('user');
            $userData = array(
                'name' => strip_tags($name),
                'email' => $email->toString(),
                'password' => UserModel::hash($password),
            );
            $userId = $model->save($userData);

            if ($userId) {
                $userData['id'] = $userId;
                Auth::write($userData);
                $this->redirect('list', 'comment');
            }
        }
    }
}
