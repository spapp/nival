<?php
/**
 *
 */
class Application {
    /**
     * @var Config application config
     */
    protected $config = null;

    /**
     * @var array apllication databases
     */
    protected $databases = array();

    /**
     * Constructor.
     *
     * @param string $aConfig config file path
     */
    public function __construct($aConfig) {
        $this->config = new Config($aConfig);
        if ($this->config->database) {
            $this->_initDatabases($this->config->database->toArray());
        }
    }

    /**
     * @return Config|null
     */
    public function config() {
        return $this->config;
    }

    /**
     * init the aplication databases
     *
     * @param array $aDbConfig databases configs
     *
     * @return void
     */
    protected function _initDatabases($aDbConfig) {
        foreach ($aDbConfig as $name => $config) {
            $this->databases[$name] = Db::factory($config['adapter'], $config['params']);
        }
    }

    /**
     * @return array
     */
    protected function getDatabases() {
        return $this->databases;
    }

    /**
     * Retunrns controller name.
     *
     * @param $aControllerName
     *
     * @return string
     */
    protected function getControllerName($aControllerName) {
        return ucfirst($aControllerName) . 'Controller';
    }

    /**
     * run the aplication
     *
     * @return void
     */
    public function run() {
        $request = new Request($_POST, $_GET);
        $controllerName =
            $this->getControllerName($request->getControllerName($this->config()->controller->defaultController));

        Loader::loadFile($this->config()->controller->path . DIRECTORY_SEPARATOR . $controllerName . '.php');

        new $controllerName($request, $this->getDatabases(), $this->config());
    }
}
