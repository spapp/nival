<?php

class Auth {
    const AUTH_KEY = 'Auth';

    const DEFAULT_ADAPER = 'Auth_Adapter_Session';

    protected static $adapter = null;

    protected function __construct() {
    }

    public static function write($aData) {
        return self::getAdapter()->write($aData);
    }

    public static function get($aName) {
        return self::getAdapter()->get($aName);
    }

    public static function authenticated() {
        return self::getAdapter()->authenticated();
    }

    public static function clear() {
        return self::getAdapter()->clear();
    }

    public static function getAdapter() {
        if (null === self::$adapter) {
            self::setAdapter(self::DEFAULT_ADAPER);
        }

        return self::$adapter;
    }

    public static function setAdapter($aAdaperName) {
        if (is_string($aAdaperName)) {
            self::$adapter = new $aAdaperName(self::AUTH_KEY);
        } elseif ($aAdaperName instanceof Auth_Adapter_Interface) {
            self::$adapter = $aAdaperName;
        } else {
            // FIXME throw a error
        }
    }
}
