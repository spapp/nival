<?php

interface Auth_Adapter_Interface {
    public function write($aData);

    public function get($aName);

    public function authenticated();

    public function clear();

    public function hasSet($aName);
}
