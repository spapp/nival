<?php

class Auth_Adapter_Session implements Auth_Adapter_Interface {
    protected $ns = 'Auth';

    public function __construct($aNs) {
        session_start();
        $this->ns = $aNs;
    }

    public function write($aData) {
        $_SESSION[$this->ns] = $aData;
        return $this;
    }

    public function get($aName) {
        if ($this->hasSet($aName)) {
            return $_SESSION[$this->ns][$aName];
        }

        return null;
    }

    public function hasSet($aName) {
        return isset($_SESSION[$this->ns][$aName]);
    }

    public function authenticated() {
        return isset($_SESSION[$this->ns]);
    }

    public function clear() {
        unset($_SESSION[$this->ns]);

        return $this;
    }
}
