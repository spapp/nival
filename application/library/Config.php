<?php
class Config {
    /**
     * @var array config values
     */
    protected $config = array();

    /**
     * @param string|array $aConfig config file path or config value
     *
     * @return void
     */
    public function __construct($aConfig) {
        if (is_string($aConfig)) {
            $this->config = include ($aConfig);
        } elseif (is_array($aConfig)) {
            $this->config = $aConfig;
        } else {
            // FIXME: saját error objektum (?)
            throw new Error('Invalid parameter type.');
        }
    }

    public function __get($aName) {
        $result = null;
        if (array_key_exists($aName, $this->config)) {
            if (is_array($this->config[$aName])) {
                $result = new Config($this->config[$aName]);
            } else {
                $result = $this->config[$aName];
            }
        }
        return $result;
    }

    public function __isset($aName) {
        return isset($this->config[$aName]);
    }

    /**
     * @return array config values
     */
    public function toArray() {
        return $this->config;
    }

    public function __toString() {
        return (string)$this;
    }
}
