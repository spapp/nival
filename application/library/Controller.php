<?php
class Controller {
    /**
     * @var Config application config
     */
    protected $config = null;

    /**
     * @var null|View
     */
    protected $view = null;

    /**
     * @var array apllication databases
     */
    protected $databases = array();

    /**
     * @var Request
     */
    protected $request = null;

    protected $models = array();

    /**
     * Constructor.
     *
     * @param Request $aRequest
     * @param array   $aDatabases
     * @param Config  $aConfig
     */
    public function __construct(Request $aRequest, array $aDatabases, Config $aConfig) {
        $this->request = $aRequest;
        $this->config = $aConfig;
        $this->databases = $aDatabases;

        $this->view = new View($this->config);
        $this->view->request = $this->request;
        $this->view->config = $this->config;

        $this->init();

        $actionFn = $this->request->getActionName($this->config()->controller->defaultAction) . 'Action';

        if (method_exists($this, $actionFn)) {
            $this->{$actionFn}();
        }

        $this->view->render($this->config()->layout->default, $this->config()->layout->path);
    }

    /**
     * @fixme
     *
     * @param string $aName
     */
    public function db($aName = '?') {

    }

    public function getModel($aName) {
        $aName = ucfirst($aName);
        if (!isset($this->models[$aName])) {
            $className = $aName . 'Model';
            Loader::loadFile('models/Abstract.php');
            Loader::loadFile('models/' . $aName . '.php');
            $this->models[$aName] = new $className($this->databases);
        }

        return $this->models[$aName];
    }

    /**
     * Returns application config.
     *
     * @return Config|null
     */
    public function config() {
        return $this->config;
    }

    /**
     * Controller initial function.
     */
    public function init() {
    }

    public function redirect($aActionName, $aControllerName = null) {
        if ($aControllerName) {
            header('Location: ' . $this->request->getUrl($aActionName, $aControllerName));
            exit;
        }

        $actionFn = $aActionName . 'Action';
        $this->request->setActionName($aActionName);
        $this->$actionFn();
    }
}
