<?php
/**
 * static database factory class
 */
class Db {
    protected function __construct() {
    }
    /**
     * @param string $aAdapter database type
     * @param array $aConfig database config
     * @return Db_<adapter>_Adapter database adapter
     */
    public static function factory($aAdapter, $aConfig) {
        // FIXME: checking params
        $class = 'Db_' . ucfirst($aAdapter) . '_Adapter';
        return new $class($aConfig);
    }
}
?>