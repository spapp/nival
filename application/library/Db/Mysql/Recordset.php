<?php
/**
 * Mysql datbase recordset object
 */
class Db_Mysql_Recordset implements Countable, Iterator {
    /**
     * mysql query pointer
     *
     * @var resurce
     */
    protected $resurce = null;

    protected $index = 0;

    /**
     * @param resurce $aResurce mysql query pointer
     */
    public function __construct($aResurce) {
        $this->resurce = $aResurce;
        $this->next();
    }

    /**
     * Get record count
     *
     * @return number
     */
    public function count() {
        if ($this->resurce) {
            return mysql_num_rows($this->resurce);
        }
        return 0;
    }

    /**
     * Get the current record.
     *
     * @return mixed
     */
    public function current() {
        return mysql_fetch_object($this->resurce);
    }

    /**
     *
     */
    public function key() {
        return $this->index;
    }

    /**
     * Get next record.
     *
     * @return boolean
     */
    public function next() {
        return ++$this->index;
    }

    /**
     * @todo not implemented
     */
    public function rewind() {
        $this->index = 0;
    }

    /**
     * The current recod validation.
     *
     * @return boolean
     */
    public function valid() {
        return ($this->index < $this->count());
    }
}
