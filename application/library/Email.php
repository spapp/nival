<?php

class Email {
    /**
     * @var string
     */
    protected $mailbox = '';
    /**
     * @var string
     */
    protected $domain = '';

    /**
     * @param string $aEmail e-mail address
     */
    public function __construct($aEmail = null) {
        if ($aEmail) {
            $this->setEmail($aEmail);
        }
    }

    /**
     * Returns e-mail address local part.
     *
     * @return string
     */
    public function getMailbox() {
        return $this->mailbox;
    }

    /**
     * Set e-mail address local part.
     *
     * @param string $aMailbox
     *
     * @return $this
     */
    public function setMailbox($aMailbox) {
        $this->mailbox = $aMailbox;
        return $this;
    }

    /**
     * Check e-mail address local part.
     *
     * @return bool
     */
    public function isValidMailbox() {
        $mailbox = $this->getMailbox();
        $len = strlen($mailbox);

        if ($len < 1 or $len > 64) {
            return false;
        } elseif ($mailbox[0] == '.' or $mailbox[$len - 1] == '.') {
            // local part starts or ends with '.'
            return false;
        } elseif (preg_match('/\\.\\./', $mailbox)) {
            // local part has two consecutive dots
            return false;
        } elseif (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $mailbox))) {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $mailbox))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns e-mail address domain part.
     *
     * @return string
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * Set e-mail address domain part.
     *
     * @param string $aDomain
     *
     * @return $this
     */
    public function setDomain($aDomain) {
        $this->domain = $aDomain;
        return $this;
    }

    /**
     * Check e-mail address domain part.
     *
     * @return bool
     */
    public function isValidDomain() {
        $domain = $this->getDomain();
        $len = strlen($domain);

        if ($len < 1 or $len > 255) {
            return false;
        } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
            // character not valid in domain part
            return false;
        } else if (preg_match('/\\.\\./', $domain)) {
            // domain part has two consecutive dots
            return false;
        } elseif (!(checkdnsrr($domain, 'MX') or checkdnsrr($domain, 'A'))) {
            // domain not found in DNS
            return false;
        }
        return true;
    }

    /**
     * Returns true if e-mail address is valid.
     *
     * @return bool
     */
    public function isValid() {
        return ($this->isValidMailbox() and  $this->isValidDomain());
    }

    /**
     * Overload.
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }

    /**
     * Returns e-mail address.
     *
     * @return string
     */
    public function toString() {
        return $this->getMailbox() . '@' . $this->getDomain();
    }

    /**
     * Set and parse e-mail address.
     *
     * @param $aEmail
     *
     * @return $this
     */
    public function setEmail($aEmail) {
        $email = explode('@', $aEmail, 2);

        if (count($email) == 2) {
            $this->setMailbox($email[0]);
            $this->setDomain($email[1]);
        }
        return $this;
    }


}