<?php
/**
 * Static Loader class
 */
class Loader {
    const PHP_FILE_SUFFIX = 'php';

    /**
     * Constuctor.
     */
    protected function __constuct() {
    }

    /**
     * Initializate the application's autoload function.
     */
    public static function initAutoLoader() {
        spl_autoload_register('Loader::autoLoad');
    }

    /**
     * The application autoload function.
     *
     * @fixme
     *
     * @param string $aName class, interface name
     *
     * @return void
     */
    public static function autoLoad($aName) {
        $filename = self::getFilePath($aName);
        include $filename;
        // FIXME: hibakezelés, ha mégse töltődne

    }

//    public static function fileExists($aName) {
//        $includePaths = explode(PATH_SEPARATOR,get_include_path());
//        $file = self::getFilePath($aName);
//
//        foreach($includePaths as $path){
//            if(file_exists($path.DIRECTORY_SEPARATOR.$file)){
//                return true;
//            }
//        }
//
//        return false;
//    }
    /**
     * Load a file.
     *
     * @fixme
     *
     * @param string $aName
     *
     * @return void
     */
    public static function loadFile($aName) {
        if (file_exists($aName)) {
            $filename = $aName;
        } else {
            $filename = APPLICATION_PATH . '/' . $aName;
        }
        include_once $filename;
        // FIXME: hibakezelés, ha mégse töltődne

    }

    /**
     * @param $aName
     *
     * @return string
     */
    protected static function getFilePath($aName) {
        return str_replace('_', DIRECTORY_SEPARATOR, $aName) . '.' . self::PHP_FILE_SUFFIX;
    }
}
