<?php

class Request {
    const CONTROLLER_KEY_NAME = 'c';

    const ACTION_KEY_NAME = 'a';

    /**
     * post params
     *
     * @var array
     */
    protected $post = null;

    /**
     * get params
     *
     * @var array
     */
    protected $get = null;

    /**
     * Constructor.
     *
     * @param array $aPostParams
     * @param array $aGetParams
     */
    public function __construct(array $aPostParams, array $aGetParams) {
        $this->post = $aPostParams;
        $this->get = $aGetParams;
    }

    /**
     * Returns the current controller name.
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getControllerName($aDefault = null) {
        return $this->getParam(self::CONTROLLER_KEY_NAME, $aDefault);
    }

    /**
     * Returns the current action name.
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getActionName($aDefault = null) {
        return $this->getParam(self::ACTION_KEY_NAME, $aDefault);
    }

    /**
     * @param $aActionName
     */
    public function setActionName($aActionName) {
        unset($this->get[self::ACTION_KEY_NAME]);
        $this->post[self::ACTION_KEY_NAME] = $aActionName;
    }


    /**
     * Retuns a parameter value.
     *
     * @param string $aName    parameter name
     * @param mixed  $aDefault default value
     *
     * @return mixed
     */
    public function getParam($aName, $aDefault = null) {
        $param = $this->getGet($aName);

        if (null === $param) {
            $param = $this->getPost($aName, $aDefault);
        }

        return $param;
    }

    /**
     * Retuns a post parameter value.
     *
     * @param string $aName    parameter name
     * @param mixed  $aDefault default value
     *
     * @return mixed
     */
    public function getPost($aName, $aDefault = null) {
        if (isset($this->post[$aName])) {
            return $this->post[$aName];
        }

        return $aDefault;
    }

    /**
     * Retuns a get parameter value.
     *
     * @param string $aName    parameter name
     * @param mixed  $aDefault default value
     *
     * @return mixed
     */
    public function getGet($aName, $aDefault = null) {
        if (isset($this->get[$aName])) {
            return $this->get[$aName];
        }

        return $aDefault;
    }

    /**
     * @param string $aActionName
     * @param string $aControllerName
     * @todo
     *
     * @return string
     */
    public function getUrl($aActionName = null, $aControllerName = null) {
        $url = ($_SERVER['SERVER_PORT'] == 80 ? 'http' : 'https');
        $url .= '://' . $_SERVER['HTTP_HOST'] . '?';
        $url .= Request::CONTROLLER_KEY_NAME . '=' . $aControllerName;
        $url .= '&'.Request::ACTION_KEY_NAME . '=' . $aActionName;

        return $url;
    }
}
