<?php
class View {
    const DEFAULT_ADAPTER = 'View_Adapter_Phtml';
    const HELPER_CLASS_PREFIX = 'View_Helper_';

    /**
     * @var array
     */
    protected $helpers = array();

    /**
     * @var Config application config
     */
    protected $config = null;

    /**
     * @var array variables container
     */
    protected $vars = array();
    /**
     * @var null
     */
    protected $adapter = null;

    /**
     * Constructor.
     *
     * @param Config $aConfig application config
     */
    public function __construct($aConfig) {
        $this->config = $aConfig;

        if (isset($aConfig->view->adapter)) {
            $this->setAdapter($aConfig->view->adapter);
        }
    }

    /**
     * Returns application config.
     *
     * @return Config|null
     */
    public function config() {
        return $this->config;
    }

    /**
     * Add a variable the view
     *
     * @see $this->vars
     *
     * @param string $aName  variable name
     * @param mixed $aValue  variable value
     *
     * @return void
     */
    public function __set($aName, $aValue) {
        $this->vars[$aName] = $aValue;
    }

    /**
     * Get a variable value
     *
     * @param string $aName variable name
     *
     * @return mixed|null
     */
    public function __get($aName) {
        if (array_key_exists($aName, $this->vars)) {
            return $this->vars[$aName];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getVariables() {
        return $this->vars;
    }

    /**
     * Get a css link
     *
     * @todo link end tag depend on doctype
     * @return string stylesheet links
     */
    public function css() {
        $css = $this->config()->view->css;

        if ($css instanceof Config) {
            $css = $css->toArray();
        } else {
            $css = (array)$css;
        }

        $output = array();

        foreach ($css as $f) {
            array_push($output, '<link href="' . $f . '" type="text/css" rel="stylesheet">');
        }

        return join(PHP_EOL, $output);
    }

    /**
     * Overload function.
     *
     * @param string $aName
     * @param array $aArgs
     *
     * @return $this|mixed|View_Helper_Abstract
     */
    public function __call($aName, $aArgs) {
        $return = $this->getHelper($aName);

        if ($return instanceof View_Helper_Abstract) {
            if (method_exists($return, $aName)) {
                return call_user_func_array(array(
                        $return,
                        $aName
                    ),
                    $aArgs);
            } else {
                return $return;
            }
        }

        return $this;
    }

    /**
     * Overload function.
     *
     * @return string
     */
    public function __toString() {
        return get_class($this);
    }

    /**
     * Returns a helper.
     *
     * @param string $aName helper name
     *
     * @return View_Helper_Abstract
     */
    public function getHelper($aName) {
        $name = $this->getHelperName($aName);
        if (!isset($this->helpers[$name])) {
            $this->addHelper($name);
        }

        return $this->helpers[$name];
    }

    /**
     * @param string $aName helper name
     *
     * @return bool
     */
    public function hasHelper($aName) {
        $name = $this->getHelperName($aName);

        return isset($this->helpers[$name]);
    }

    /**
     * Add a helper.
     *
     * @param string|View_Helper_Abstract $aHelper
     *
     * @return $this
     * @throws View_Exception
     */
    public function addHelper($aHelper) {
        if (is_string($aHelper)) {
            $aHelper = new $aHelper($this);
        }

        if ($aHelper instanceof View_Helper_Abstract) {
            $this->helpers[(string)$aHelper] = $aHelper;
        } else {
            throw new View_Exception('Not valid helper.');
        }

        return $this;
    }

    /**
     * Render a view.
     *
     * @param string $aFile
     * @param string $aPath
     *
     * @return void
     */
    public function render($aFile, $aPath = '') {
        if ($aPath) {
            $path = $aPath;
        } else {
            $path = $this->config()->view->path;
        }

        $file = $this->getAdapter()->render($aFile, $path);

        if (is_string($file)) {
            include $file;
        }
    }

    /**
     * @return View_Adapter_Abstract
     */
    public function getAdapter() {
        if (null === $this->adapter) {
            $this->setAdapter();
        }
        return $this->adapter;
    }

    /**
     * @param string $aAdapter
     *
     * @return $this
     */
    public function setAdapter($aAdapter = self::DEFAULT_ADAPTER) {
        $this->adapter = new $aAdapter($this);
        return $this;
    }

    /**
     * Returns a helper class name.
     *
     * @param string $aName
     *
     * @return string
     */
    protected function getHelperName($aName) {
        return self::HELPER_CLASS_PREFIX . ucfirst($aName);
    }
}
