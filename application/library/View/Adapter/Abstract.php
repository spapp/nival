<?php
/**
 * Interface for view adapters.
 */
abstract class View_Adapter_Abstract {
    /**
     * @var View
     */
    protected $view = null;

    /**
     * @param View $aView
     */
    public function __construct(View $aView) {
        $this->setView($aView);
    }

    /**
     * @param View $aView
     *
     * @return $this
     */
    public function setView(View $aView) {
        $this->view = $aView;
        return $this;
    }

    /**
     * @return null|View
     */
    public function getView() {
        return $this->view;
    }

    /**
     *
     * @param string $aFile
     * @param string $aPath
     *
     */
    public abstract function render($aFile, $aPath);
}