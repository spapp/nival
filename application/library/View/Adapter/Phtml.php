<?php
class View_Adapter_Phtml extends View_Adapter_Abstract {
    const TEMPLATE_SUFFIX = 'phtml';

    /**
     * Render a view.
     *
     * @param string $aFile
     * @param string $aPath
     *
     * @return void
     */
    public function render($aFile, $aPath) {
        return $aPath . DIRECTORY_SEPARATOR . $aFile . '.' . self::TEMPLATE_SUFFIX;
    }
}