<?php

define('SMARTY_DIR', APPLICATION_PATH . '/library/Smarty/');

require_once(APPLICATION_PATH . '/library/Smarty/Smarty.class.php');

class View_Adapter_Smarty extends View_Adapter_Abstract {
    const TEMPLATE_SUFFIX = 'tpl';

    protected $smarty = null;

    public function __construct(View $aView) {
        parent::__construct($aView);
        $this->setSmarty();
    }

    public function setSmarty(Smarty $aSmarty = null) {
        if(null === $aSmarty){
            $aSmarty = new Smarty();
            $aSmarty->setTemplateDir($this->getView()->config()->view->path);
            $aSmarty->setCompileDir('/tmp');
            //$aSmarty->setConfigDir('/web/www.example.com/guestbook/configs/');
            $aSmarty->setCacheDir('/tmp');
        }

        $this->smarty = $aSmarty;
    }

    public function getSmarty() {
        return $this->smarty;
    }

    /**
     * Render a view.
     *
     * @param string $aFile
     * @param string $aPath
     *
     * @return void
     */
    public function render($aFile, $aPath) {
        $file = $aPath . DIRECTORY_SEPARATOR . $aFile . '.' . self::TEMPLATE_SUFFIX;
        $vars = $this->getView()->getVariables();
        $smarty = $this->getSmarty();

        foreach($vars as $name=>$value){
            $smarty->assign($name, $value);
        }

        $smarty->display($file);

        return false;
    }
}