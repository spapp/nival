<?php

abstract class  View_Helper_Abstract {
    /**
     * @var View
     */
    protected $view = null;

    /**
     * @param View $aView
     */
    public function __construct(View $aView) {
        $this->setView($aView);
    }

    /**
     * @param View $aView
     */
    public function setView(View $aView) {
        $this->view = $aView;
    }

    /**
     * @return null|View
     */
    public function getView() {
        return $this->view;
    }

    /**
     * @return string
     */
    public function __toString() {
        return get_class($this);
    }
}
