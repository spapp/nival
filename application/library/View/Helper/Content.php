<?php
class View_Helper_Content extends View_Helper_Abstract {
    /**
     * @return void
     */
    public function content() {
        $file = $this->view->request->getActionName($this->view->config->controller->defaultAction);
        $path = $this->view->config->view->path . DIRECTORY_SEPARATOR .
                $this->view->request->getControllerName($this->view->config->controller->defaultController);

        $this->view->render($file, $path);
    }
}
