<?php
abstract class AbstractModel {
    /**
     * @var array apllication databases
     */
    protected $databases = array();

    protected $table = null;

    protected $tableName = null;

    public function __construct($aDatabases) {
        $this->databases = $aDatabases;
    }

    public function fetchAll() {
        $sql = 'SELECT * FROM ' . $this->tableName;

        return $this->getTable()->select($sql);
    }

    public function save($aValues) {
        if (isset($data['id'])) {
            return $this->getTable()->update($aValues);
        } else {
            return $this->getTable()->insert($aValues);
        }
    }


    public function fetchOne($where) {
        $sql = 'SELECT * FROM ' . $this->tableName;
        $sqlWhere = array();

        foreach ($where as $key => $value) {
            array_push($sqlWhere, $key . '= %' . $key);
        }

        if (count($sqlWhere) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $sqlWhere);
        }

        $res = $this->getTable()->select($sql, $where);

        return $res->current();
    }

    protected function getDb($aName = 'master') {
        if ($aName === 'slave') {
            $aName = 'slave' . time() % 2;
        }

        return $this->databases[$aName];
    }

    protected function getTable() {
        if (null == $this->table) {
            $this->table = new Db_Table($this->tableName, $this->getDb('slave'));
        }

        return $this->table;
    }

}
