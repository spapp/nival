<?php
class UserModel extends AbstractModel {
    const SALT = 'jFjUIUhwru434weRnjfd';
    protected $tableName = 'user';

    public static function hash($aString) {
        return sha1(self::SALT . $aString);
    }
}
