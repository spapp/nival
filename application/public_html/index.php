<?php
/**
 * Define path to application directory.
 * @global string APPLICATION_PATH
 */
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(dirname((__FILE__)))));
/**
 * Define application environment.
 * @global string APPLICATION_ENV
 */
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV'):'app'));
/**
 * Set up error display.
 */
ini_set('display_errors', (APPLICATION_ENV=='development'));
/**
 * Typically, you will also want to add your library/ directory to the include_path.
 */
set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/library',
    get_include_path(),
)));
/**
 * register autoloader
 */
require_once 'Loader.php';
Loader::initAutoLoader();
/**
 * Create application and run
 */
$application = new Application(
    APPLICATION_PATH . '/configs/default.php'
);

$application->run();
