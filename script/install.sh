#!/bin/sh
#

SCRIPT_FILE=`readlink -f $0`
SCRIPT_PATH=`dirname $SCRIPT_FILE`


if [ "${1}" = "-h" ]; then
    echo "USAGE"
    echo "\tinstall.sh [ DB_USER DB_PWD]"
    echo "\t\tDB_USER\t mysql database username (default: root)"
    echo "\t\tDB_PWD\t mysql database password (default: 123)"
    exit 0
fi

${SCRIPT_PATH}/make_db.sh ${*}

if [ $? -eq 1 ]; then
    exit 1
fi

echo "For make_virtual_host.sh"

sudo ${SCRIPT_PATH}/make_virtual_host.sh

if [ $? -eq 1 ]; then
    exit 1
fi

exit 0
