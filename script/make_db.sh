#!/bin/sh
#

SCRIPT_FILE=`readlink -f $0`
SCRIPT_PATH=`dirname $SCRIPT_FILE`

# db config
DB_HOST="localhost"
DB_USER="root"
DB_PWD="123"
DB_NAME="nival"
DB_PORT="3306"
DB_SCHEMA="db.sql"

if [ $# -eq 2 ]; then
    DB_USER="${1}"
    DB_PWD="${2}"
fi

DB_CMD="mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PWD"
SQL_CREATE_DATABASE="CREATE DATABASE ${DB_NAME} DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"

# create database
IS_EXISTS_DB=`${DB_CMD} -e "SHOW DATABASES" | grep -c ${DB_NAME}`

if [ ${IS_EXISTS_DB} -lt 1 ]; then
    ${DB_CMD} -e "${SQL_CREATE_DATABASE}"
    if [ $? -eq 0 ]; then
        echo "Create database ('${DB_NAME}') success."
    else
        echo "Create database ('${DB_NAME}') unsuccess."
        exit 1
    fi
else
    echo "'${DB_NAME}' database already exists."
    exit 0
fi

# use schema
${DB_CMD} -D "${DB_NAME}" < "${SCRIPT_PATH}/${DB_SCHEMA}"

if [ $? -eq 0 ]; then
    echo "${DB_NAME} schema using is complete."
else
    echo "${DB_NAME} schema using is not complete."
    exit 1
fi

exit 0
